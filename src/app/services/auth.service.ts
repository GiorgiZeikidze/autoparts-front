import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl = environment.apiUrl;
  

  constructor(
    private http: HttpClient
  ) { }


  login(user: any) {

    return this.http
               .post(`${this.apiUrl}auth/signin`, user)
            
  }; 

  isLoggedIn() {

    return !!localStorage.getItem('token'); 
  };
  
  getToken() {

    return localStorage.getItem('token'); 
  }







}
