import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor( 
        private injector: Injector,
        private router: Router
    ) { }

  
  intercept(req, next) {

    let authService = this.injector.get(AuthService)
    let tokenizedReq = req.clone({
      setHeaders:{
        Authorization: authService.getToken()
      }
    }); 

    if ( this.router.url !== 'login' ) {
      
      return next.handle(tokenizedReq); 
    }; 
  }
}
