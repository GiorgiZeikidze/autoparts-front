import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { UtileService } from '../shared/services/utile.service';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private _utileService: UtileService
  ) { }


  getUsers(): Observable<any> {
    return this.http
               .get(`${this.apiUrl}users`);
  }; 

  // getIfUserExists(): Observable<any> {

  //   return this.http
  //              .get(`${this.apiUrl}user-exists?username=${this._utileService.getUserName()}`);

  // }; 
}
