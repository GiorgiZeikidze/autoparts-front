import { Component, OnInit } from '@angular/core';
import { UserProfileService } from 'src/app/services/user-profile.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(
    private _userProfService: UserProfileService,
    private _router: Router
  ) { }

  ngOnInit(): void {

    this.getUsers();
  }


  getUsers() {

    return this._userProfService
               .getUsers()
               .subscribe( res => {

                console.log(res);
                
               }, err => {

                console.log(err);
                
               }); 
  }; 

}
