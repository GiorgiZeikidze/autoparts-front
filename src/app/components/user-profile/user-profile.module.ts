import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserProfileService } from 'src/app/services/user-profile.service';
import { TokenInterceptorService } from 'src/app/services/token-interceptor.service';

const COMPONENTS = [

  UserProfileComponent
]

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    HttpClientModule
  ], 
  providers:[
    UserProfileService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class UserProfileModule { }
