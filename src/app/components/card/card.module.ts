import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardRoutingModule } from './card-routing.module';
import { FailComponent } from './fail/fail.component';
import { CardComponent } from './card.component';
import { SuccessComponent } from './success/success.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    CardComponent,
    SuccessComponent,
    FailComponent
  ],
  imports: [
    CommonModule,
    CardRoutingModule,
    SharedModule
  ]
})
export class CardModule { }
