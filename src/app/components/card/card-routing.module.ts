import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardComponent } from './card.component';
import { SuccessComponent } from './success/success.component';
import { FailComponent } from './fail/fail.component';

const routes: Routes = [
  { 
    path: '', component: CardComponent, children:[
      // { path: '', redirectTo: 'success', pathMatch: 'full' } ,
      { path: 'success', component: SuccessComponent },
      { path: 'fail', component: FailComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardRoutingModule { }







