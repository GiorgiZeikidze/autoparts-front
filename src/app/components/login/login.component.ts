import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  hide = true;
  isLoading = false; 

  constructor(
    private fb: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) { 

    this.loginForm = this.fb.group({

      username: [ '', Validators.required ],
      password: [ '', Validators.required ]

    })
  }

  ngOnInit(): void {

  }



  onSubmit(form: any) {

      if ( form.value['username'].length > 0 && form.value['password'].length > 0)  {

        console.log(form.value);
        this.isLoading = true;

        this._authService
            .login(form.value)
            .subscribe(res => {
              
              this.isLoading = false; 
              localStorage.setItem('token', res['accessToken'] );
              localStorage.setItem('userName', form.value['username']); 
              this._router.navigate(['/user-profile']);
              console.log(res);
              

            }, err => {

              console.log(err);
              this.isLoading = false; 
              
            }); 
      };


    
  }

}
