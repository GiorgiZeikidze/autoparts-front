import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtileService {

  constructor() { }


  getUserName() {

    return localStorage.getItem('userName'); 
  }
}
