import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { UtileService } from './services/utile.service';


const COMPONENTS = [

  AngularMaterialModule
]


@NgModule({
  declarations: [ PageNotFoundComponent ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  providers: [
    UtileService
  ], 
  exports: [
    COMPONENTS
  ]
})
export class SharedModule { }
