import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { AuthGuard } from './guard/auth.guard';


const routes: Routes = [

  { 
    path: '', 
    redirectTo: '/login', 
    pathMatch: 'full' 
  }, 

  //  & & & Lazy Load * * * *
  { 
    path: 'login', 
    loadChildren: './components/login/login.module#LoginModule' 
  }, 
  
  {  
    path: 'user-profile', 
    loadChildren: './components/user-profile/user-profile.module#UserProfileModule', 
    canActivate: [AuthGuard] 
  }, 

  {
    path: 'card',
    loadChildren: './components/card/card.module#CardModule'
  }, 

  //  * * * 404 * ** 
  { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
